import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as Nano from 'nano';

@Injectable()
export class NanoService {
  readonly client;

  constructor(private configService: ConfigService) {
    this.client = Nano(
      this.configService.get('COUCH_PROTOCOL') +
        '://' +
        this.configService.get('COUCH_USER') +
        ':' +
        this.configService.get('COUCH_PASSWORD') +
        '@' +
        this.configService.get('COUCH_HOST'),
    );
  }

  async getDbList(): Promise<string[]> {
    return this.client.db.list();
  }
}
