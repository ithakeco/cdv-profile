import { UserProfile } from '../profile/documents/userProfile.document';
import { Injectable } from '@nestjs/common';

import * as Nano from 'nano';
import { UserOrgs } from '../profile/documents/userOrgs.document';
import { NanoService } from './nanoClient.service';

const UserProfileDocName = 'personal';
const UserOrgsDocName = 'userOrgs';

@Injectable()
export class UserDbService {
  nanoClient;
  constructor(private nanoService: NanoService) {
    this.nanoClient = this.nanoService.client;
  }

  getUserDb(userId) {
    const dbName = this.usernameToDbName(userId);
    return this.nanoClient.db.use(dbName);
  }

  async getUserDbInfo(userId) {
    const dbName = this.usernameToDbName(userId);
    const info = await this.nanoClient.db.get(dbName);
    return info;
  }

  async getUserOrgs(userId: string): Promise<UserOrgs> {
    const userDb = this.getUserDb(userId);
    return userDb.get(UserOrgsDocName);
  }

  async getUserProfile(userId: string): Promise<UserProfile> {
    const userDb = this.getUserDb(userId);
    return userDb.get(UserProfileDocName);
  }

  /**
   * add or update personal document. should include the _rev token to update
   */
  async upsertUserProfile({
    userId,
    data,
  }: {
    userId: string;
    data: UserProfile;
  }): Promise<Nano.DocumentInsertResponse> {
    const userDb = this.getUserDb(userId);
    return userDb.insert(data, UserProfileDocName);
  }

  /**
   * add or update a userOrgs. should include the _rev token to update
   */
  async upsertUserOrgs({
    userId,
    data,
  }: {
    userId: string;
    data: UserOrgs;
  }): Promise<Nano.DocumentInsertResponse> {
    const userDb = this.getUserDb(userId);
    return userDb.insert(data, UserOrgsDocName);
  }

  /**
   * Create a db for the user
   * @returns the name of the created db
   */
  async createUserDb(userId): Promise<string> {
    const userDbName = this.usernameToDbName(userId);
    await this.nanoClient.db.create(userDbName);
    return userDbName;
  }

  usernameToDbName(userId: string) {
    return 'userdb-' + Buffer.from(userId).toString('hex');
  }
}
