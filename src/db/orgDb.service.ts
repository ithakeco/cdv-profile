import { Injectable } from '@nestjs/common';

import * as Nano from 'nano';
import { OrgProfile } from '../profile/documents/orgProfile.document';
import { v4 as uuidv4 } from 'uuid';
import { NanoService } from './nanoClient.service';

const OrgProfileDocumentName = 'personal';

@Injectable()
export class OrgDbService {
  nanoClient;
  constructor(private nanoService: NanoService) {
    this.nanoClient = this.nanoService.client;
  }

  getOrgDbName(orgId) {
    return 'orgdb_' + orgId;
  }

  /**
   * @returns Returns a database object
   */
  getOrgDb(orgId) {
    const orgDbName = this.getOrgDbName(orgId);
    return this.nanoClient.db.use(orgDbName);
  }

  async getOrgProfile(orgId: string): Promise<OrgProfile> {
    const orgDb = this.getOrgDb(orgId);
    return orgDb.get(OrgProfileDocumentName);
  }

  async upsertOrgProfile({
    orgId,
    data,
  }): Promise<Nano.DocumentInsertResponse> {
    const orgDb = this.getOrgDb(orgId);
    return orgDb.insert(data, OrgProfileDocumentName);
  }

  /**
   * @returns the uuid used in the name of the created db
   */
  async createOrgDb(): Promise<string> {
    const orgId = uuidv4();
    const orgDbName = this.getOrgDbName(orgId);
    await this.nanoClient.db.create(orgDbName);
    return orgId;
  }
}
