import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';

import { OrgProfile } from './documents/orgProfile.document';
import { UserOrgs } from './documents/userOrgs.document';
import { UserProfile } from './documents/userProfile.document';
import { FrenchAddress } from './dto/frenchAddress.dto';
import {
  CreateOrgProfileDto,
  OrgProfileDto,
  UpdateOrgProfileDto,
} from './dto/orgProfile.dto';
import { OrgStatus } from './dto/orgStatus.dto';
import {
  CreateUserProfileDto,
  UpdateUserProfileDto,
  UserProfileDto,
} from './dto/userProfile.dto';
import {
  BadRequestError,
  BadRequestErrorMessage,
} from './errors/badRequest.error';
import {
  ForbiddenError,
  ForbiddenErrorMessage,
} from './errors/forbidden.error';
import { NotFoundError, NotFoundErrorMessage } from './errors/notFound.error';
import { plainToInstance } from 'class-transformer';
import { omitBy } from 'lodash';
import { firstValueFrom } from 'rxjs';
import { UserDbService } from '../db/userDb.service';
import { OrgDbService } from '../db/orgDb.service';
import { OrgDto } from './dto/orgs.dto';

@Injectable()
export class ProfileService {
  constructor(
    private readonly httpService: HttpService,
    private readonly userDbService: UserDbService,
    private readonly orgDbService: OrgDbService,
  ) {}

  async createUserProfile(
    userId: string,
    createUserProfileDto: CreateUserProfileDto,
  ): Promise<boolean> {
    const userProfileToAdd = plainToInstance(
      UserProfile,
      createUserProfileDto,
      {
        excludeExtraneousValues: true,
      },
    );

    const sanitizedUserProfileToAdd = omitBy(
      userProfileToAdd,
      (v) => v === undefined,
    );

    if (sanitizedUserProfileToAdd.frenchAddress != null) {
      sanitizedUserProfileToAdd.frenchAddress =
        await this.validateFrenchAddress(createUserProfileDto.frenchAddress);
    }

    try {
      // Access to the user profile data
      await this.userDbService.getUserProfile(userId);
      return true;
    } catch (e) {
      if (e instanceof Error) {
        // case 1 : no datapod
        if (e.message == 'Database does not exist.') {
          console.log('Creating datapod');
          await this.userDbService.createUserDb(userId);
          console.log('Initializing user');
          await this.userDbService.upsertUserProfile({
            userId,
            data: userProfileToAdd,
          });
        }
        // case 2 : the datapod exists but the personal infos are missing
        else if (e.message == 'missing') {
          await this.userDbService.upsertUserProfile({
            userId,
            data: userProfileToAdd,
          });
        } else {
          // unknown error
          throw e;
        }
      } else {
        // unknown error
        throw e;
      }

      return true;
    }
  }

  async updateUserProfile(
    userId: string,
    updateUserProfileDto: UpdateUserProfileDto,
  ): Promise<boolean> {
    const userProfileToUpdate = plainToInstance(
      UserProfile,
      updateUserProfileDto,
      {
        excludeExtraneousValues: true,
      },
    );

    const sanitizedUserProfileToUpdate = omitBy(
      userProfileToUpdate,
      (v) => v === undefined,
    );

    if (sanitizedUserProfileToUpdate.frenchAddress != null) {
      sanitizedUserProfileToUpdate.frenchAddress =
        await this.validateFrenchAddress(updateUserProfileDto.frenchAddress);
    }

    const userProfile = await this.userDbService.getUserProfile(userId);

    await this.userDbService.upsertUserProfile({
      userId,
      data: { ...userProfile, ...sanitizedUserProfileToUpdate },
    });

    return true;
  }

  async createUserOrganization(
    userId: string,
    createOrgProfileDto: CreateOrgProfileDto,
  ): Promise<string> {
    const orgProfileToAdd = plainToInstance(OrgProfile, createOrgProfileDto, {
      excludeExtraneousValues: true,
    });

    if (orgProfileToAdd.frenchAddress != null) {
      orgProfileToAdd.frenchAddress = await this.validateFrenchAddress(
        createOrgProfileDto.frenchAddress,
      );
    }

    try {
      // Access to the already created orgs
      const userOrgs: UserOrgs = await this.userDbService.getUserOrgs(userId);
      return userOrgs.orgsIds[0];
    } catch (e) {
      // Organization creation
      if (e.message == 'missing' || e.message == 'deleted') {
        const orgId = await this.orgDbService.createOrgDb();
        orgProfileToAdd.ownerId = userId;
        await this.orgDbService.upsertOrgProfile({
          orgId,
          data: orgProfileToAdd,
        });

        const userOrgs = new UserOrgs([]);
        userOrgs.orgsIds.push(orgId);
        await this.userDbService.upsertUserOrgs({
          userId,
          data: userOrgs,
        });

        return orgId;
      } else {
        throw e;
      }
    }
  }

  async updateUserOrganization(
    userId: string,
    updateOrgProfileDto: UpdateOrgProfileDto,
  ): Promise<boolean> {
    const orgProfileToUpdate = plainToInstance(
      OrgProfile,
      updateOrgProfileDto,
      {
        excludeExtraneousValues: true,
      },
    );

    const sanitizedOrgProfileToUpdate = omitBy(
      orgProfileToUpdate,
      (v) => v === undefined,
    );

    const userOrgs = await this.userDbService.getUserOrgs(userId);
    const orgId = userOrgs.orgsIds[0];

    const orgProfile = await this.orgDbService.getOrgProfile(orgId);

    sanitizedOrgProfileToUpdate.ownerId = userId;

    await this.orgDbService.upsertOrgProfile({
      orgId,
      data: { ...orgProfile, ...sanitizedOrgProfileToUpdate },
    });
    return true;
  }

  async validateFrenchAddress(
    frenchAddress: FrenchAddress,
  ): Promise<FrenchAddress> {
    if (frenchAddress == null) {
      throw new BadRequestError(BadRequestErrorMessage.ADDRESS_UNEXISTS);
    }

    const q = encodeURIComponent(frenchAddress.label);
    const type = encodeURIComponent(frenchAddress.type);
    const postcode = encodeURIComponent(frenchAddress.postcode);
    const citycode = encodeURIComponent(frenchAddress.citycode);

    const checkedAddress = await firstValueFrom(
      this.httpService.get(
        'https://api-adresse.data.gouv.fr/search/?q=' +
          q +
          '&type=' +
          type +
          '&postcode=' +
          postcode +
          '&citycode=' +
          citycode +
          '&autocomplete=0"',
      ),
    );

    const promise: Promise<FrenchAddress> = new Promise((resolve, reject) => {
      checkedAddress.data.features.forEach((feature) => {
        if (feature.properties.id == frenchAddress.id) {
          // We found the ID, so we do a normalization
          const ret = new FrenchAddress();
          ret.id = feature.properties.id;
          ret.citycode = feature.properties.citycode;
          ret.postcode = feature.properties.postcode;
          ret.city = feature.properties.city;
          ret.context = feature.properties.context;
          ret.label = feature.properties.label;
          ret.geometry = feature.geometry;
          ret.name = feature.properties.name;
          ret.type = feature.properties.type;

          switch (ret.type) {
            case 'housenumber':
              ret.housenumber = feature.properties.housenumber;
            case 'street':
              ret.street = feature.properties.street;
          }

          resolve(ret);
        }
      });
      reject('Address not found');
    });
    return promise;
  }

  async getUserProfile(userId: string): Promise<UserProfileDto> {
    try {
      // Access to the user profile data
      const userProfile = await this.userDbService.getUserProfile(userId);
      return plainToInstance(UserProfileDto, userProfile, {
        excludeExtraneousValues: true,
      });
    } catch (e) {
      if (e instanceof Error) {
        // case 1 : no datapod or no personal informations inside
        if (e.message == 'Database does not exist.' || 'missing') {
          throw new NotFoundError(NotFoundErrorMessage.USER_DATAPOD_NOT_FOUND, {
            cause: e,
          });
        } else {
          // unknown error
          throw e;
        }
      } else {
        // unknown error
        throw e;
      }
    }
  }

  async getOrgProfile(userId: string): Promise<OrgProfileDto> {
    try {
      const userOrgs = await this.userDbService.getUserOrgs(userId);
      const orgId = userOrgs.orgsIds[0];
      const orgProfile = await this.orgDbService.getOrgProfile(orgId);
      return plainToInstance(
        OrgProfileDto,
        { ...orgProfile, orgId },
        { excludeExtraneousValues: true },
      );
    } catch (e) {
      if (e instanceof Error) {
        // case 1 : no datapod or no personal informations inside
        if (e.message == 'Database does not exist.' || 'missing') {
          throw new NotFoundError(NotFoundErrorMessage.USER_DATAPOD_NOT_FOUND, {
            cause: e,
          });
        } else {
          // unknown error
          throw e;
        }
      } else {
        // unknown error
        throw e;
      }
    }
  }

  async getOrgs(userId: string): Promise<OrgDto[]> {
    try {
      const userOrgs = await this.userDbService.getUserOrgs(userId);
      const orgPromises = userOrgs.orgsIds.map(
        async (orgId): Promise<OrgDto> => {
          const orgProfile = await this.orgDbService.getOrgProfile(orgId);
          return { orgId, orgName: orgProfile.name };
        },
      );
      return await Promise.all(orgPromises);
    } catch (e) {
      if (e instanceof Error) {
        // case 1 : no datapod or no personal informations inside
        if (e.message == 'Database does not exist.' || 'missing') {
          throw new NotFoundError(NotFoundErrorMessage.USER_DATAPOD_NOT_FOUND, {
            cause: e,
          });
        } else {
          // unknown error
          throw e;
        }
      } else {
        // unknown error
        throw e;
      }
    }
  }

  async getOrgStatus(userId: string, orgId: string): Promise<OrgStatus> {
    const orgPersonal = await this.orgDbService.getOrgProfile(orgId);

    if (userId !== orgPersonal.ownerId) {
      throw new ForbiddenError(ForbiddenErrorMessage.NOT_OWNER_ORG_ID);
    }

    return {
      orgId,
      type: orgPersonal.type,
    };
  }
}
