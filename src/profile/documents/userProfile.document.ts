import { FrenchAddress } from '../dto/frenchAddress.dto';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class UserProfile {
  @Expose()
  firstName: string;
  @Expose()
  lastName: string;
  @Expose()
  birthdate: string;
  @Expose()
  address: string;
  @Expose()
  avatar: number;
  @Expose()
  ecoLevel: number;
  @Expose()
  dataUsage: boolean;
  @Expose()
  frenchAddress: FrenchAddress;
  @Expose()
  interests: number[];
}
