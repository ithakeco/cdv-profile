import { FrenchAddress } from '../dto/frenchAddress.dto';
import { Exclude, Expose } from 'class-transformer';

export enum OrgProfileType {
  ASSOCIATION = 'ASSOCIATION',
  COMPANY = 'COMPANY',
}
@Exclude()
export class OrgProfile {
  @Expose()
  siret: number;
  @Expose()
  name: string;
  @Expose()
  frenchAddress: FrenchAddress;
  @Expose()
  address: string;
  @Expose()
  email: string;
  @Expose()
  interests: number[];
  @Expose()
  phone: string;
  @Expose()
  motivation: string;
  @Expose()
  website: string;
  @Expose()
  facebook: string;
  @Expose()
  instagram: string;
  @Expose()
  x: string;
  @Expose()
  type: OrgProfileType;
  @Expose()
  ownerId: string;
}
