import { NanoService } from './../db/nanoClient.service';
import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';
import { UserDbService } from '../db/userDb.service';
import { OrgDbService } from '../db/orgDb.service';

@Module({
  imports: [ConfigModule, HttpModule],
  providers: [ProfileService, NanoService, UserDbService, OrgDbService],
  controllers: [ProfileController],
})
export class ProfileModule {}
