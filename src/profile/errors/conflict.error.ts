export const ConflictErrorMessage = {
  ORG_USER_CONFLICT:
    "The organization to update could'nt be updated (user orgs != 1)",
} as const;

export class ConflictError extends Error {
  constructor(message?: string, options?: ErrorOptions) {
    super(message, options);
  }
}
