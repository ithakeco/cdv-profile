export const NotFoundErrorMessage = {
  USER_DATAPOD_NOT_FOUND: 'User datapod was not found',
} as const;

export class NotFoundError extends Error {
  constructor(message?: string, options?: ErrorOptions) {
    super(message, options);
  }
}
