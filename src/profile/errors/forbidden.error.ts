export const ForbiddenErrorMessage = {
  NOT_OWNER_ORG_ID: 'You don’t have permission to access to this org id',
} as const;

export class ForbiddenError extends Error {
  constructor(message?: string, options?: ErrorOptions) {
    super(message, options);
  }
}
