export const BadRequestErrorMessage = {
  ADDRESS_UNEXISTS: 'The provided french address cound not be verified',
} as const;

export class BadRequestError extends Error {
  constructor(message?: string, options?: ErrorOptions) {
    super(message, options);
  }
}
