import { ApiProperty, PickType } from '@nestjs/swagger';

import { OrgProfileType } from '../documents/orgProfile.document';
import { FrenchAddress } from './frenchAddress.dto';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  ArrayMaxSize,
  Contains,
  IsArray,
  IsEmail,
  IsEnum,
  IsInt,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUrl,
  Matches,
  Max,
  MaxLength,
  Min,
  ValidateNested,
} from 'class-validator';

@Exclude()
export class OrgProfileBase {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsNumber()
  @Expose()
  siret: number;

  @ApiProperty({ required: false, maxLength: 200 })
  @IsOptional()
  @IsString()
  @MaxLength(200)
  @Expose()
  name: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @ValidateNested()
  @Type(() => FrenchAddress)
  @Expose()
  frenchAddress: FrenchAddress;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  @MaxLength(200)
  @Expose()
  address: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsEmail()
  @Expose()
  email: string;

  @ApiProperty({ required: false, type: [Number] })
  @IsOptional()
  @IsArray()
  @ArrayMaxSize(20)
  @Max(19, { each: true })
  @Min(0, { each: true })
  @IsInt({ each: true })
  @Expose()
  interests: number[];

  @ApiProperty({ required: false })
  @IsOptional()
  @IsPhoneNumber()
  @Expose()
  phone: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  @Expose()
  motivation: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsUrl()
  @Expose()
  website: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsUrl()
  // https://amirdiafi.medium.com/validate-all-facebook-url-schemes-e0736d7bdbda
  @Matches(
    /^(?:(?:http|https):\/\/)?(?:www\.)?(?:facebook|fb|m\.facebook)\.(?:com|me)\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]+)(?:\/)?/i,
    {
      message:
        'not valid facebook url, supported urls: fb.com, facebook.com, m.fb.com ',
    },
  )
  @Expose()
  facebook: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsUrl()
  @Contains('instagram')
  @Expose()
  instagram: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsUrl()
  @Contains('twitter')
  @Expose()
  x: string;

  @ApiProperty({
    enum: OrgProfileType,
  })
  @IsEnum(OrgProfileType)
  @Expose()
  type: OrgProfileType;

  @ApiProperty()
  @Expose()
  ownerId: string;
}

export class CreateOrgProfileDto extends PickType(OrgProfileBase, [
  'siret',
  'name',
  'frenchAddress',
  'address',
  'email',
  'interests',
  'phone',
  'motivation',
  'website',
  'facebook',
  'instagram',
  'x',
  'type',
] as const) {}

export class UpdateOrgProfileDto extends PickType(OrgProfileBase, [
  'website',
  'facebook',
  'instagram',
  'x',
] as const) {}

@Exclude()
export class OrgProfileDto extends PickType(OrgProfileBase, [
  'siret',
  'name',
  'frenchAddress',
  'address',
  'email',
  'phone',
  'website',
  'facebook',
  'instagram',
  'x',
  'type',
  'ownerId',
] as const) {
  @ApiProperty()
  @Expose()
  orgId: string;
}
