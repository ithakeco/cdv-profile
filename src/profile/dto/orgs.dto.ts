import { ApiProperty } from '@nestjs/swagger';

import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class OrgDto {
  @ApiProperty()
  @Expose()
  orgId: string;

  @ApiProperty()
  @Expose()
  orgName: string;
}
