import { ApiProperty } from '@nestjs/swagger';

import { MaxLength, IsString, IsDefined } from 'class-validator';

export class FrenchAddress {
  @ApiProperty()
  @IsString()
  @IsDefined()
  @MaxLength(20)
  id: string;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(200)
  label: string;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(50)
  type: string;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(10)
  citycode: string;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(10)
  postcode: string;

  @ApiProperty({ required: false })
  @IsString()
  @MaxLength(200)
  name: string;

  @ApiProperty({ required: false })
  @IsString()
  @MaxLength(200)
  street: string;

  @ApiProperty({ required: false })
  @IsString()
  @MaxLength(200)
  context: string;

  @ApiProperty({ required: false })
  @IsString()
  @MaxLength(10)
  housenumber: string;

  @ApiProperty({ required: false })
  @IsString()
  @MaxLength(100)
  city: string;

  @ApiProperty({ required: false })
  geometry: Geometry;
}

export interface Geometry {
  type: string;
  coordinates: any[];
}

export class Point implements Geometry {
  type: 'Point';
  coordinates: number[];
}

export class Line implements Geometry {
  type: 'Line';
  coordinates: number[][];
}

export class MultiLine implements Geometry {
  type: 'MultiLine';
  coordinates: number[][];
}

export class Polygon implements Geometry {
  type: 'Polygon';
  coordinates: number[][][];
}

export class MultiPolygon implements Geometry {
  type: 'MultiPolygon';
  coordinates: number[][][][];
}
