import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class OrgStatus {
  @ApiProperty()
  @Expose()
  orgId: string;

  @ApiProperty()
  @Expose()
  type: string;
}
