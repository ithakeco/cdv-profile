import { ApiProperty, PickType } from '@nestjs/swagger';

import { FrenchAddress } from './frenchAddress.dto';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsDateString,
  Min,
  Max,
  MaxLength,
  IsBoolean,
  ValidateNested,
  IsArray,
  ArrayMaxSize,
  IsInt,
  IsOptional,
} from 'class-validator';

@Exclude()
export class UserProfileBase {
  @ApiProperty({ required: false })
  @IsOptional()
  @MaxLength(50)
  @Expose()
  firstName: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @MaxLength(50)
  @Expose()
  lastName: string;

  @ApiProperty({ required: false, format: 'date' })
  @IsOptional()
  @IsDateString()
  @Expose()
  birthdate: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @MaxLength(200)
  @Expose()
  address: string;

  @ApiProperty({ required: false, type: Number, minimum: 0, maximum: 19 })
  @IsOptional()
  @Min(0)
  @Max(19)
  @IsInt()
  @Expose()
  avatar: number;

  @ApiProperty({ required: false, type: Number, minimum: 1, maximum: 4 })
  @IsOptional()
  @Min(1)
  @Max(4)
  @IsInt()
  @Expose()
  ecoLevel: number;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsBoolean()
  @Expose()
  dataUsage: boolean;

  @ApiProperty({ required: false })
  @IsOptional()
  @ValidateNested()
  @Type(() => FrenchAddress)
  @Expose()
  frenchAddress: FrenchAddress;

  @ApiProperty({ required: false, type: [Number] })
  @IsOptional()
  @IsArray()
  @ArrayMaxSize(20)
  @Max(19, { each: true })
  @Min(0, { each: true })
  @IsInt({ each: true })
  @Expose()
  interests: number[];
}
export class CreateUserProfileDto extends PickType(UserProfileBase, [
  'firstName',
  'lastName',
  'birthdate',
  'address',
  'avatar',
  'ecoLevel',
  'dataUsage',
  'frenchAddress',
  'interests',
] as const) {}

export class UpdateUserProfileDto extends PickType(UserProfileBase, [
  'firstName',
  'lastName',
  'birthdate',
  'address',
  'dataUsage',
  'frenchAddress',
] as const) {}

export class UserProfileDto extends PickType(UserProfileBase, [
  'firstName',
  'lastName',
  'birthdate',
  'address',
  'dataUsage',
  'frenchAddress',
] as const) {}
