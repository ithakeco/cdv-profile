import {
  Body,
  ConflictException,
  Controller,
  ForbiddenException,
  Get,
  Logger,
  NotFoundException,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import {
  CreateOrgProfileDto,
  OrgProfileDto,
  UpdateOrgProfileDto,
} from './dto/orgProfile.dto';
import { OrgStatus } from './dto/orgStatus.dto';
import {
  CreateUserProfileDto,
  UpdateUserProfileDto,
  UserProfileDto,
} from './dto/userProfile.dto';
import { ConflictError } from './errors/conflict.error';
import { ForbiddenError } from './errors/forbidden.error';
import { ProfileService } from './profile.service';
import { QueryRequired } from '@app/cdv-api-common/decorators/queryRequired.decorator';
import { UserInfo } from '@app/cdv-api-common/decorators/userInfo.decorator';
import { NotFoundError } from './errors/notFound.error';
import { OrgDto } from './dto/orgs.dto';

@Controller('profile')
@ApiTags('profile')
export class ProfileController {
  private readonly logger = new Logger(ProfileController.name);

  constructor(private readonly profileService: ProfileService) {}

  @Get('org-status')
  @ApiOkResponse({
    status: 200,
    type: OrgStatus,
  })
  getUserOrgStatus(
    @UserInfo() userinfo,
    @QueryRequired('orgId') orgId: string,
  ): Promise<OrgStatus> {
    return this.profileService.getOrgStatus(userinfo.id, orgId).catch((err) => {
      if (err instanceof ForbiddenError) {
        throw new ForbiddenException(err.message, { cause: err });
      } else {
        throw err;
      }
    });
  }

  @Post('user')
  @ApiOkResponse({
    status: 200,
    description: 'Creation status',
    type: Boolean,
  })
  async createUserProfile(
    @Body() userProfile: CreateUserProfileDto,
    @UserInfo() userinfo,
  ): Promise<boolean> {
    return this.profileService.createUserProfile(userinfo.id, userProfile);
  }

  @Patch('user')
  @ApiOkResponse({
    status: 200,
    description: 'Update status',
    type: Boolean,
  })
  async updateUserProfile(
    @Body() userProfile: UpdateUserProfileDto,
    @UserInfo() userinfo,
  ): Promise<boolean> {
    return this.profileService.updateUserProfile(userinfo.id, userProfile);
  }

  @Get('user')
  @ApiOkResponse({
    status: 200,
    description: 'User profile',
    type: UserProfileDto,
  })
  getUserProfile(@UserInfo() userinfo): Promise<UserProfileDto> {
    return this.profileService.getUserProfile(userinfo.id).catch((err) => {
      if (err instanceof NotFoundError) {
        throw new NotFoundException(err.message, { cause: err });
      } else {
        throw err;
      }
    });
  }

  @Get('org')
  @ApiOkResponse({
    status: 200,
    description: 'Org profile',
    type: OrgProfileDto,
  })
  getOrgProfile(@UserInfo() userinfo): Promise<OrgProfileDto> {
    return this.profileService.getOrgProfile(userinfo.id).catch((err) => {
      if (err instanceof NotFoundError) {
        throw new NotFoundException(err.message, { cause: err });
      } else {
        throw err;
      }
    });
  }

  @Get('orgs')
  @ApiOkResponse({
    status: 200,
    description: 'Orgs',
    isArray: true,
    type: OrgDto,
  })
  getOrgs(@UserInfo() userinfo): Promise<OrgDto[]> {
    return this.profileService.getOrgs(userinfo.id).catch((err) => {
      if (err instanceof NotFoundError) {
        throw new NotFoundException(err.message, { cause: err });
      } else {
        throw err;
      }
    });
  }

  @Post('org')
  @ApiOkResponse({
    status: 201,
    description: 'Org profile id',
    type: String,
  })
  async createUserOrganization(
    @UserInfo() userinfo,
    @Body() createOrgProfileDto: CreateOrgProfileDto,
  ): Promise<string> {
    return this.profileService
      .createUserOrganization(userinfo.id, createOrgProfileDto)
      .catch((err) => {
        if (err instanceof ConflictError) {
          throw new ConflictException(err.message, { cause: err });
        } else {
          throw err;
        }
      });
  }

  @Patch('org')
  @ApiOkResponse({
    status: 200,
    description: 'Update status',
    type: String,
  })
  async updateUserOrganization(
    @UserInfo() userinfo,
    @Body() updateOrgProfileDto: UpdateOrgProfileDto,
  ): Promise<boolean> {
    return this.profileService
      .updateUserOrganization(userinfo.id, updateOrgProfileDto)
      .catch((err) => {
        if (err instanceof ConflictError) {
          throw new ConflictException(err.message, { cause: err });
        } else {
          throw err;
        }
      });
  }
}
