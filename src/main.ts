import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { GlobalExceptionFilter } from '@app/cdv-api-common';

async function bootstrap() {
  process.on('uncaughtException', function (err) {
    console.error(err);
    console.log('Node NOT Exiting...');
  });
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('Profile data service')
    .setDescription('This service manages users data pods')
    .setVersion('1.0')
    .addTag('profile')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  });
  app.useGlobalFilters(new GlobalExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  await app.listen(3000);
}
bootstrap();
