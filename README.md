## Prerequisites

- A running instance of CouchDB with a database \_users and "couch_peruser" option enabled [link](https://docs.couchdb.org/en/stable/config/couch-peruser.html)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
